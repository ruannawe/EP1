#ifndef IMAGEM_H
#define IMAGEM_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;
class Imagem
{
private:

char *vln;
string numeroMagico, comentario;
int largura, altura, nivelCinza;

public:
    void setOpc(char *opc);
    void setNumeroMagico(string numeroMagico);   
    void setComentario(string comentario);   
    void setLargura(int largura);   
    void setAltura(int altura);
    void setNivelCinza(int nivelCinza);   
    void setNegativo(char *vln);
};






#endif
