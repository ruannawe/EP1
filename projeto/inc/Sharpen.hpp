#ifndef SHARPEN_H
#define SHARPEN_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

class Sharpen
{
private:
int largura, altura;
char *vl, *vm;
public:
void setLargura(int largura);
void setAltura(int altura);
void setSharpen(char *vl, char *vm);
char *getVmSp();
};




#endif
