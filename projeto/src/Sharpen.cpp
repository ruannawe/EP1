#include "Sharpen.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <ostream>
#include <istream>
using namespace std;
void Sharpen::setSharpen(char *vl, char *vm)
{
int valor, x, y, j, div=1, i=0, opc=0, filtro[9]; 
    
    for(i=0;i<9;i++)
    {

        if(i==0 || i==2 || i==6 || i==8)
        {
        filtro[i]=0;
        }

        if(i==1 || i==3 || i==5 || i==7)
        {
        filtro[i]=-1;
        }

        if(i==4 )
        {
        filtro[i]=5;
        }

    }


    for(i=1; i<largura-1; i++)
    {
		for(j=1; j<altura-1; j++)
		{
			valor = 0;
			for(x=-1; x<=1; x++)
			{
				for(y=-1; y<=1;y++)
				{
					valor += filtro[3*(x+1)+(y+1)]*(unsigned char)vl[(i+x)*largura+(y+j)];
				}
			}
				valor /= div;
				
				valor = valor < 0 ? 0 : valor;
				valor = valor > 255 ? 255 : valor;
				
				vm[i*largura+j] = valor;
		}
    }

this -> vm=vm;
}


void Sharpen::setLargura(int largura)
{
this -> largura=largura;
}

void Sharpen::setAltura(int altura)
{
this -> altura=altura;
}

char *Sharpen::getVmSp(){
	return vm;
}


