#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Imagem.hpp"
#include "Sharpen.hpp"
#include "Smoot.hpp"
using namespace std;

int main(){

    //variáveis
    int i=0, opc=0; 

    //var 2
    int nivelCinza, altura, largura;
    //
    
    string numeroMagico, comentario;
    char *vl, *vm;

    //declarando classes
    Imagem img; 
    Sharpen sp;
    Smoot sm;
    
    //
    ifstream leitor;
    //ofstream newImgFile("lena-2.pgm");
    //abrindo a imagem
    leitor.open("imagem.pgm");


    //lendo o numero mágico e atribuindo da variável numeroMagico
    getline(leitor, numeroMagico);
	//atribuindo a variável numeroMagico na nova imagem que será gerada
	img.setNumeroMagico(numeroMagico);
	
	//lendo o comentário, e atribuindo na variável comentario
	getline(leitor, comentario);
	//atribuindo a variável comentario na nova imagem que será gerada
	img.setComentario(comentario);
	
	//atribuindo a altura e largura e o nivel de cinza nas respectivas variáveis, largura, altura, nivelCinza
	leitor>>largura>>altura>>nivelCinza;
	//atribuindo a nova imagem a ser formada
	img.setAltura( altura);
	img.setLargura( largura);
	img.setNivelCinza( nivelCinza);
	      
	vl=new char[largura*altura];
	vm=new char[largura*altura];

	for(i=0;i<largura*altura;i++)
    {
    leitor.get(vl[i]);
    vl[i]=(unsigned char)vl[i];
    vm[i]=(unsigned char)(vl[i]);
    }
    
//começo a brincar

cout<<"Digite 0 para aplicar o filtro sharpen, 1 para aplicar o filtro smoot ou 2 para aplicar o negativo\n";
cin>>opc;

if(opc==1)
{
    sm.setLargura(largura);
    sm.setAltura(altura);
    sm.setSmoot(vl,vm);
    img.setOpc(sm.getVmSm());    

}

if(opc==0)
{
    sp.setLargura(largura);
    sp.setAltura(altura);
    sp.setSharpen(vl, vm);
    img.setOpc(sp.getVmSp());    
}

if(opc==2)
{
    img.setNegativo(vm);        
}

leitor.close();
	
return 0;
}
